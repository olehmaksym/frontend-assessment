/* eslint-disable @next/next/no-img-element */
import { redirect } from 'next/navigation'
import { ParamInterface } from '@/app/interfaces'

async function getData(id: string) {
  const res = await fetch(`${process.env.APIpath}/api/favorite-items/${id}`)
  if (!res.ok) {
    throw new Error('Failed to fetch data')
  }

  return res.json()
}

async function Item({ params }: ParamInterface) {
  const data = await getData(params.id);
  if (data.error) {
    redirect('/dashboard/list')
  }

  const { item } = data

  return <div className='flex flex-col'>
    <div className='flex items-center mt-[20px]'>
      <img src={item.image} alt={item.name} width='55px' height='55px' className='object-contain mr-[20px]' />
      <div className='flex flex-col'>
        <h1 className='font-semibold text-[16px]'>{item.name}</h1>
        <span className='text-[12px] text-gray'>{item.brand}</span>
      </div>
    </div>
    <section className='flex flex-col bg-white rounded-[5px] mt-[20px] pt-[20px] pb-[30px] h-auto min-h-[400px] flex shadow-[0px_0px_12px_rgba(0,0,0,0.05)]'>
      <div className='pb-[20px] px-[40px] border-solid border-silver border-b'>
        <h2 className='font-semibold text-[16px] mb-[5px]'>Product Information</h2>
        <p className='text-[12px] text-gray'>Item and seller details</p>
      </div>
      <div className='px-[40px] py-[20px] flex flex-wrap text-[14px]'>
        {[
          { title: 'Brand', value: item.brand},
          { title: 'Model Name', value: item.model},
          { title: 'Color', value: item.color},
          { title: 'Category', value: item.category},
        ].map(i => (
          <div className='w-[50%] mb-[25px]' key={i.title}>
            <div className='text-gray mb-[5px]'>{i.title}</div>
            <div>{i.value}</div>
          </div>
        ))}
        <div className='w-full'>
          <div className='text-gray mb-[5px]'>About</div>
          <div>{item.description}</div>
        </div>
      </div>
    </section>
  </div>;
}

export default Item
