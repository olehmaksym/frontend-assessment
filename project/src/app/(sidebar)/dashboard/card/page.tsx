'use client'
import useSWR from 'swr'
import Image from 'next/image'
import Cart from './cart'

import { fetcher } from '@/app/helpers'
import { ItemInterface } from '@/app/interfaces'
import Loader from '@/app/components/Loader'

export default function DashboardCard() {
  const { data, error, isLoading } = useSWR('/api/favorite-items', fetcher)
  if (error) return <div>Failed to load</div>
  if (isLoading) return <div className='mt-[40px]'><Loader/></div>

  return (
    <>
      <h1 className='font-bold text-[24px]'>Dashboard</h1>
      <div className='flex flex-wrap'>
        {data.data.map((item: ItemInterface) => <Cart key={item.id} item={item} />)}
      </div>
    </>
  )
}
