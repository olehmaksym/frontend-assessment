/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import { ItemInterface } from "@/app/interfaces";
import Tag from "@/app/components/Tag";

interface ItemProps {
  item: ItemInterface;
}

const Cart: React.FC<ItemProps> = ({ item }) => (
  <div className="w-[calc(25%-50px)] m-[25px] bg-white rounded-[5px] flex flex-col items-center border-solid border-silver border relative">
    <img
      src={item.image}
      alt={item.name}
      width="85px"
      height="85px"
      className="object-contain aspect-square my-[20px]"
    />
    <div className="px-[20px] mb-[10px] font-semibold text-center">
      {item.name}
    </div>
    <div className="mb-[10px] text-gray2">{`$${item.price}`}</div>
    <Tag isActive={item.active} className="mb-[60px]" />
    <div className="flex w-full absolute bottom-0 font-semibold text-center">
      <div className="border-solid border-silver border-t border-r w-[50%] text-red py-[10px]">
        Remove
      </div>
      <Link
        href={`/item/${item.id}`}
        className="border-solid border-silver border-t w-[50%] py-[10px] cursor-pointer"
      >
        View
      </Link>
    </div>
  </div>
);

export default Cart;
