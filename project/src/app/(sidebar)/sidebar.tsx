import Link from 'next/link';
import Image from 'next/image';
import { FiInbox } from 'react-icons/fi';
import { AiOutlineHome } from 'react-icons/ai';

const menuItems = [
  {
    title: 'Dashboard',
    link: '/dashboard/list',
    icon: AiOutlineHome
  },
  {
    title: 'Card',
    link: '/dashboard/card',
    icon: FiInbox
  },
]

export default function Navbar() {
  return (
    <div className='bg-darkblue py-[20px] px-[10px] text-white flex flex-col w-[250px] relative'>
      <Image
        src="/logo.svg"
        alt="Logo"
        className="ml-[10px] mb-[20px]"
        width={32}
        height={32}
        priority
      />
      <nav className='flex flex-col'>
        {menuItems.map(m => (
          <Link
            href={m.link}
            key={m.link}
            className={`p-[10px] mx-[5px] rounded-[5px] hover:bg-shadowblue flex items-center`}
          >
            <m.icon/>
            <span className='ml-[10px]'>{m.title}</span>
          </Link>
        ))}
      </nav>
      <div className='bg-lightblue absolute bottom-0 left-0 right-0 p-[20px] flex items-center'>
        <Image
          src="/face.png"
          alt="Logo"
          className="rounded-full"
          width={45}
          height={45}
        />
        <div className='ml-[20px]'>
          <p className='text-[14px] leading-[14px] font-medium'>Tom Cook</p>
          <span className='text-[12px]'>View profile</span>
        </div>
      </div>
    </div>
  )
}
