import Sidebar from '@/app/(sidebar)/sidebar'

export default function DashboardLayoutSidebar({
  children
}: {
  children: React.ReactNode,
}) {
  return (
    <main className='h-full flex'>
      <Sidebar />
      <div className='p-[30px] w-full'>
        {children}
      </div>
    </main>
  );
}
