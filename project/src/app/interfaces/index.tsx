export interface ItemInterface {
  id: string;
  name: string;
  image: string;
  price: string;
  seller: string;
  active: boolean;
}

export interface ParamInterface {
  params: {
    id: string;
  }
}
