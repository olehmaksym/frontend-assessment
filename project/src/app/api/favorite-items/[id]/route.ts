import { NextResponse } from 'next/server'
import { ParamInterface } from '@/app/interfaces'

import data from '../../../../../data.json'

export async function GET(request: Request, { params }: ParamInterface) {
  const item = data.find(e => e.id === params.id)

  if (!item) {
    return NextResponse.json({
      error: `Item with id: ${params.id} not found!`
    })
  }

  return NextResponse.json({ item })
}
