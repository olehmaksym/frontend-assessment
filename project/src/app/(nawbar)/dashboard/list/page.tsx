'use client'
import useSWR from 'swr'
import Image from 'next/image';
import { fetcher } from '@/app/helpers'
import Loader from '@/app/components/Loader'
import Cart from './cart'

export default function DashboardList() {
  const { data, error, isLoading } = useSWR('/api/favorite-items', fetcher)
  if (error) return <div>Failed to load</div>
  if (isLoading) return <div className='mt-[40px]'><Loader/></div>

  return (
    <>
      <h2 className='text-[14px] font-semibold mb-[10px]'>Favorites</h2>
      <p className='text-[12px] font-medium text-gray'>A list of your favorites items to keep track of.</p>
      <div className='flex flex-col mt-[30px]'>
        <div className='flex justify-between text-[14px] font-semibold border-solid border-silver border-b py-[10px]'>
          <span className='w-[40%]'>Name</span>
          <span className='w-[24%]'>Seller</span>
          <span className='w-[12%] text-center'>Status</span>
          <span className='w-[12%] text-center'>Price</span>
          <span className='w-[12%]'></span>
        </div>
        {data.data.map((item: any) => <Cart key={item.id} item={item} />)}
      </div>
    </>
  )
}
