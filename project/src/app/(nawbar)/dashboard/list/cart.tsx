/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import { ItemInterface } from '@/app/interfaces'
import Tag from '@/app/components/Tag'

interface ItemProps {
  item: ItemInterface
}

const Cart: React.FC<ItemProps> = ({ item }) => {
  return (
    <div className='flex justify-between text-[14px] border-solid border-silver border-b py-[10px] items-center'>
      <Link href={`/item/${item.id}`} className='w-[40%] flex items-center cursor-pointer hover:underline'>
        <img src={item.image} alt={item.name} width='40px' height='40px' className='object-contain' />
        <span className='ml-[15px]'>{item.name}</span>
      </Link>
      <span className='w-[24%]'>{item.seller}</span>
      <span className='w-[12%]'>
        <Tag isActive={item.active}/>
      </span>
      <span className='w-[12%] font-semibold text-center'>{`$${item.price}`}</span>
      <span className='w-[12%] text-red font-semibold text-right'>Remove</span>
    </div>
  );
}

export default Cart
