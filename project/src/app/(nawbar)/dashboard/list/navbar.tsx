import Link from 'next/link';
import Image from 'next/image';
import { FaBell } from 'react-icons/fa';

const menuItems = [
  {
    title: 'Dashboard',
    link: '/dashboard/list'
  },
  {
    title: 'Card',
    link: '/dashboard/card'
  },
]

export default function Navbar() {
  return (
    <div className='flex justify-between border-solid border-silver2 border-b pb-[10px] text-white'>
      <div className='flex items-center'>
        <Image
          src="/logo.svg"
          alt="Logo"
          className="ml-[10px] mr-[20px]"
          width={32}
          height={32}
          priority
        />
        <nav>
          {menuItems.map(m => (
            <Link
              href={m.link}
              key={m.link}
              className={`p-[10px] mx-[5px] rounded-[5px] hover:bg-shadowblue`}
            >
              {m.title}
            </Link>
          ))}
        </nav>
      </div>
      <div className='flex items-center'>
        <FaBell />
        <Image
          src="/face.png"
          alt="Logo"
          className="ml-[20px] rounded-full"
          width={32}
          height={32}
        />
      </div>
    </div>
  )
}
