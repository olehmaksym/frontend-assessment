import Navbar from './list/navbar'

export default function DashboardLayoutNavbar({
  children
}: {
  children: React.ReactNode,
}) {
  return (
    <main className='h-full'>
      <div className='h-[300px] bg-darkblue p-[20px]'>
        <Navbar />
        <h1 className='my-[40px] font-bold text-[24px] text-white'>Dashboard</h1>
      </div>
      <section className='bg-white rounded-[5px] mx-[20px] px-[40px] pt-[20px] pb-[30px] mt-[-120px] h-auto min-h-[400px] flex shadow-[0px_0px_12px_rgba(0,0,0,0.05)]'>
        <div className='flex flex-col w-full relative'>
          <div className='rounded-[5px] text-[14px] text-white bg-purple py-[5px] px-[10px] absolute top-[10px] right-0 cursor-pointer'>
            + Add
          </div>
          {children}
        </div>
      </section>
    </main>
  );
}
