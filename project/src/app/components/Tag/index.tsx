interface TagProps {
  isActive: boolean;
  className?: string;
}

const Tag: React.FC<TagProps> = ({ isActive, className = '' }) => isActive
  ? <div className={`bg-green text-lightgreen w-max mx-auto py-[4px] px-[8px] font-medium rounded-full ${className}`}>Available</div>
  : <div className={`bg-pink text-incarnadine w-max mx-auto py-[4px] px-[8px] font-medium rounded-full ${className}`}>Unavailable</div>

export default Tag
