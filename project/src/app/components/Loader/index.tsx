import c from'./loader.module.css'

const Loader = () => <div className={c.loader}></div>

export default Loader
