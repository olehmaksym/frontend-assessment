/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {},
    colors: {
      red: '#FF0000',
      silver: '#D0D0D0',
      silver2: '#343E4D',
      gray: '#626060',
      gray2: '#4E4E4E',
      purple: '#4F46E5',
      white: '#FFFFFF',
      darkblue: '#1F2937',
      lightblue: '#374151',
      shadowblue: '#111827',
      green: '#DCFCE7',
      lightgreen: '#166534',
      incarnadine: '#652916',
      pink: '#FCDCDC',
    }
  },
  plugins: [],
}
