This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Before Start

Make a copy of file `.env.dist` and rename it to `.env`

It's required for correct work mock API
## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## App description

The application has three pages
* List page `/dashboard/list`
* Card page `/dashboard/card`
* Detail page `/item/[id]`

You can browse through the menu to navigate to them. To navigate to the Detail pane need to press the View button or click on an item in the items list.

On the API side developed two endpoints according to the requirements.
* `/api/favorite-items` - Getting a list of favorite items
* `/api/favorite-items/[id]` - Getting details of an item from its ID. (If there is no item with the requested id, the app provides an error message)
